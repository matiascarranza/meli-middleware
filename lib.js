const _ = require('lodash')
const striptags = require('striptags')
const sanitize = require('sanitize-html')

const parseSearchQuery = (data) => {
  const itemsToProcess = _.slice(data.results, 0, 4)
  const items = _.map(itemsToProcess, parseItemInfo)
  const categories = getCategories(data)
  const author = getAuthor()

  return {
    author,
    items,
    categories
  }
}

const getAuthor = () => ({
  'name': 'Matias',
  'lastname': 'Carranza'
})

const getCategories = (data) => {
  const categoryItem = _.find(data.filters, (item) => {
    return item.id === 'category'
  })

  if (categoryItem && categoryItem.values) {
    return _.map(categoryItem.values[0].path_from_root, (category) => {
      return category.name
    })
  }

  return []
}

const parseItemInfo = (item) => {
  const price = _.split(item.price, '.')
  return {
    'id': item.id,
    'title': item.title,
    'price': {
      'currency': item.currency_id,
      'amount': price[0],
      'decimals': price[1] || '00'
    },
    'address': {
      city_name: item.address ? item.address.city_name : item.seller_address.city.name
    },
    'picture': _.isEmpty(item.pictures) ? item.thumbnail : item.pictures[0].secure_url,
    'condition': item.condition,
    'free_shipping': item.shipping.free_shipping
  }
}

const parseIndividualItem = (itemData, description) => {
  const item = _.assignIn(parseItemInfo(itemData), {
    'sold_quantity': itemData.sold_quantity,
    'description': description.plain_text || sanitize(striptags(description.text))
  })

  const author = getAuthor()

  return {
    author,
    item
  }
}

module.exports = {
  parseSearchQuery,
  getAuthor,
  getCategories,
  parseItemInfo,
  parseIndividualItem
}
