const test = require('ava')
const nock = require('nock')
const micro = require('micro')
const listen = require('test-listen')
const request = require('request-promise')

const api = require('./index')

const server = fn => listen(micro(fn))

test('search items', async t => {
  nock('https://api.mercadolibre.com')
    .get('/sites/MLA/search')
    .query({ q: 'test' })
    .replyWithFile(200, '__mocks__/items.json')

  const url = await server(api)
  const response = await request(`${url}/api/items?q=test`)
  const search = JSON.parse(response)

  t.is(search.items.length, 4)
  t.deepEqual(search.categories, [ 'Electrónica, Audio y Video', 'iPod', 'Reproductores' ])
})

test('search with one item result', async t => {
  nock('https://api.mercadolibre.com')
    .get('/sites/MLA/search')
    .query({ q: 'test' })
    .replyWithFile(200, '__mocks__/one_item_search.json')

  const url = await server(api)
  const response = await request(`${url}/api/items?q=test`)
  const search = JSON.parse(response)

  t.is(search.items.length, 1)
  t.deepEqual(search.categories, [ 'Electrónica, Audio y Video', 'iPod', 'Reproductores' ])
})

test('search without results', async t => {
  nock('https://api.mercadolibre.com')
    .get('/sites/MLA/search')
    .query({ q: 'search without results' })
    .replyWithFile(200, '__mocks__/empty_search.json')

  const url = await server(api)
  const response = await request(`${url}/api/items?q=search without results`)
  const search = JSON.parse(response)

  t.is(search.items.length, 0)
  t.deepEqual(search.categories, [ ])
})

test('get item with result', async t => {
  nock('https://api.mercadolibre.com')
    .get('/items/123')
    .replyWithFile(200, '__mocks__/item.json')

  nock('https://api.mercadolibre.com')
    .get('/items/123/description')
    .replyWithFile(200, '__mocks__/description.json')

  const url = await server(api)
  const response = await request(`${url}/api/items/123`)
  const item = JSON.parse(response)

  t.deepEqual(item, {
    author: {
      name: 'Matias',
      lastname: 'Carranza'
    },
    item: {
      id: 'MLA627480404',
      title: 'Apple Ipod Touch 32gb 6ta Generacion',
      price: { currency: 'ARS', amount: '7500', decimals: '00' },
      address: { city_name: 'Cañitas' },
      picture: 'https://mla-s1-p.mlstatic.com/110215-MLA25160971599_112016-O.jpg',
      condition: 'new',
      free_shipping: false,
      sold_quantity: 7,
      description: '\r\nApple iPod Touch 32GB 6ta Generacion\r\nDescripción\r\n\r\nDimensiones y pesoAlto: 123.4 mm (4.86 pulgadas)Ancho: 58.6 mm (2.31 pulgadas)Profundidad: 6.1 mm (0.24 pulgadas)Peso: 88 gramos (3.10 onzas)1\r\nCapacidad32 GB\r\nPantallaPantalla widescreen Multi-Touch con tecnología IPS de 4 pulgadas (diagonal)Pantalla RetinaResolución de 1136 x 640 pixeles a 326 pixeles por pulgadaRelación de contraste 800:1 (normal)Brillo máximo de 500 cd/m2 (normal)'
    }
  })
})
