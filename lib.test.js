const test = require('ava')

const {
  parseSearchQuery,
  getAuthor,
  getCategories,
  parseItemInfo,
  parseIndividualItem
} = require('./lib')

const fs = require('fs')
/* mocks for search */
const items = JSON.parse(fs.readFileSync('__mocks__/items.json', 'utf8'))
const emptySearch = JSON.parse(fs.readFileSync('__mocks__/empty_search.json', 'utf8'))

/* mocks for item */
const individaulItem = JSON.parse(fs.readFileSync('__mocks__/item.json', 'utf8'))
const description = JSON.parse(fs.readFileSync('__mocks__/description.json', 'utf8'))

/* tests */
test('parseSearchQuery with results', t => {
  const result = parseSearchQuery(items)

  t.is(result.items.length, 4)
})

test('parseSearchQuery without results', t => {
  const result = parseSearchQuery(emptySearch)

  t.is(result.categories.length, 0)
})

test('getCategories with results', t => {
  const result = getCategories(items)

  t.is(result.length, 3)
  t.deepEqual(result, [ 'Electrónica, Audio y Video', 'iPod', 'Reproductores' ])
})

test('parseItemInfo with result', t => {
  const result = parseItemInfo(items.results[0])

  t.deepEqual(result, {
    id: 'MLA627480404',
    title: 'Apple Ipod Touch 32gb 6ta Generacion',
    price: { currency: 'ARS', amount: '7500', decimals: '00' },
    address: { city_name: 'Cañitas' },
    picture: 'http://mla-s1-p.mlstatic.com/110215-MLA25160971599_112016-I.jpg',
    condition: 'new',
    free_shipping: false
  })
})

test('parseIndividualItem with result', t => {
  const result = parseIndividualItem(individaulItem, description)

  t.deepEqual(result, {
    author: {
      name: 'Matias',
      lastname: 'Carranza'
    },
    item: {
      id: 'MLA627480404',
      title: 'Apple Ipod Touch 32gb 6ta Generacion',
      price: { currency: 'ARS', amount: '7500', decimals: '00' },
      address: { city_name: 'Cañitas' },
      picture: 'https://mla-s1-p.mlstatic.com/110215-MLA25160971599_112016-O.jpg',
      condition: 'new',
      free_shipping: false,
      sold_quantity: 7,
      description: '\r\nApple iPod Touch 32GB 6ta Generacion\r\nDescripción\r\n\r\nDimensiones y pesoAlto: 123.4 mm (4.86 pulgadas)Ancho: 58.6 mm (2.31 pulgadas)Profundidad: 6.1 mm (0.24 pulgadas)Peso: 88 gramos (3.10 onzas)1\r\nCapacidad32 GB\r\nPantallaPantalla widescreen Multi-Touch con tecnología IPS de 4 pulgadas (diagonal)Pantalla RetinaResolución de 1136 x 640 pixeles a 326 pixeles por pulgadaRelación de contraste 800:1 (normal)Brillo máximo de 500 cd/m2 (normal)'
    }
  })
})

test('getAuthor', t => {
  const result = getAuthor()

  t.deepEqual(result, {
    name: 'Matias',
    lastname: 'Carranza'
  })
})
