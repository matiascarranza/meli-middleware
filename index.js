const { send } = require('micro')
const { router, get } = require('microrouter')
const fetch = require('isomorphic-fetch')
const { parseSearchQuery, parseIndividualItem } = require('./lib')

const cors = require('micro-cors')()

const getItems = async (req, res) => {
  const { q } = req.query
  const result = await fetch(`https://api.mercadolibre.com/sites/MLA/search?q=${q}`)
  const json = await result.json()

  res.setHeader('Content-Type', 'application/json; charset=utf-8')

  send(res, 200, parseSearchQuery(json))
}

const getItem = async (req, res) => {
  const { id } = req.params
  const itemResult = await fetch(`https://api.mercadolibre.com/items/${id}`)
  const descriptionResult = await fetch(`https://api.mercadolibre.com/items/${id}/description`)
  const item = await itemResult.json()
  const description = await descriptionResult.json()

  res.setHeader('Content-Type', 'application/json; charset=utf-8')
  send(res, 200, parseIndividualItem(item, description))
}

const notfound = (req, res) =>
  send(res, 404, 'Not found route')

module.exports = cors(router(
  get('/api/items', getItems),
  get('/api/items/:id', getItem),
  get('/*', notfound)
))
